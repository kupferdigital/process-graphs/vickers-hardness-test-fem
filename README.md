# vickers-hardness-test-fem
[![OntoFlow](https://gitlab.com/kupferdigital/process-graphs/vickers-hardness-test-fem/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=OntoFlow&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/vickers-hardness-test-fem)
[![Graph](https://gitlab.com/kupferdigital/process-graphs/vickers-hardness-test-fem/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=Turtle&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/vickers-hardness-test-fem/index.ttl)

A simple representation of EDX data created on Diffusion Couples at fem Forschungsinstitut

You can use this jupyter_notebook [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://gitlab.com/kupferdigital/process-graphs/vickers-hardness-test-fem/-/raw/main/create_dataset.ipynb) to interact with the [KupferDigital Ckan](https://ckan.kupferdigital.org) and to transform and store data.
note: You will need a valid user and api-key to use it

<img src="https://kupferdigital.gitlab.io/process-graphs/vickers-hardness-test-fem/vickers-hardness-test-fem.svg">
