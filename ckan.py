import requests
from urllib.parse import urlparse, urljoin
from io import BytesIO



class CKANSession:
    def __init__(
            self,
            ckan_url: str,
            api_token: str,
            ):
        self.url=ckan_url
        self.session= requests.Session()
        self.session.headers.update({"Authorization": api_token})
        self.groups=self.__get_groups__()
        if self.groups:
            print('connection to ckan established')
            print('available dataset groups are: {}'.format(self.groups.keys()))
        else:
            print('could not establish ckan session')

    def __get_groups__(self):
        r_groups=self.api_get('/api/action/group_list',params={'all_fields': True})['result']
        return {item['name']: item  for item in r_groups}
        
    def file_upload(self, dataset_id, filename, filedata, format='', group=None, mime_type='text/csv'):
        url=expand_url(self.url,'/api/action/resource_create')
        data_stream=BytesIO(filedata)
        # or resource_update
        data={
            "package_id": dataset_id,
            "name": filename,
            "format": format
        }
        #files=[('upload', data_stream)]
        files=[('upload', (filename, data_stream, mime_type))]
        response=self.session.post(url, data=data, files=files).json()
        resource=response['result']
        print('file {} uploaded at: {}'.format(filename, resource['url']))
        return resource

    def link_ressource(self, dataset_id, ressource_name, ressource_url, format='', group=None):
        url=self.url+'/api/action/resource_create'
        # or resource_update
        data={
            "package_id": dataset_id,
            "name": ressource_name,
            "url": ressource_url,
            "format": format
        }
        resource=self.session.post(url, data=data).json()['result']
        print('ressource {} linked at: {}'.format(ressource_name, resource['url']))
        return resource
    def api_get(self, url, params={}):
        p_url=expand_url(self.url, url)
        try:
            response = self.session.get(p_url,params=params)
            response.raise_for_status()
            r = response.json()
        except requests.exceptions.RequestException as e:
            #placeholder for save file / clean-up
            raise SystemExit(e) from None
        return r

    # def get_api_tokens():
    #     response=requests.post(ckan_url+'/api/action/api_token_list',data={'user': USER},headers={"X-CKAN-API-Key": API_KEY})
    #     #return first token
    #     return response.json()['result']

    #requests.post(ckan_url+'/api/action/package_create',data={'name': 'test2','private': True,'owner_org': ORG} ,headers={'X-CKAN-API-Key': API_KEY})
    def get_create_dataset(self, name: str, groups: list=[], owner_org: str='IWM'):
        r=self.session.post(self.url+'/api/action/package_search',data={'q': 'name:{}'.format(name)})
        results_list=r.json()['result']['results']
        #print(results_list)
        if results_list:
            #return first hit
            print('dataset exist, returning first found')
            return results_list[0]
        else:
            #create package
            url=self.url+'/api/action/package_create'
            # or resource_update
            groups_dict=[self.groups[item] for item in groups]
            data={
                "name": name,
                "owner_org": owner_org,
                "groups": groups_dict,
                "private": False
                }
            #print(url, data)
            if r.status_code==200:
                r=self.session.post(url, json=data)
                r_json=r.json()
                if 'result' in r_json.keys():
                    dataset=r.json()['result']
                    print("dataset created")
                    return dataset
            print(r.status_code,r.content)
            
    def remove_dataset(self, id):
        p_url=expand_url(self.url, '/api/action/package_delete')
        r=self.session.post(p_url,data={"id": id})
        if r.json()['success']:
            print('dataset deleted')
        return True


def expand_url(base, url):
    p_url = urlparse(url)
    if not p_url.scheme in ['https', 'http']:
        #relative url?
        p_url=urljoin(base, p_url.path)
        return p_url
    else:
        return p_url.path.geturl()



